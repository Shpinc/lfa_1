#include <iostream>
#include <string.h>
#include <string>
#include <fstream>
using namespace std;


ifstream f("data.in");

class Matricea{

    bool leg ;
    string lit;
    bool isf ;

public:
    friend class Automata;
    bool is_final(){if(isf == 1)return 1; else return 0;}
    bool is_leg(){if(leg == 1)return 1; else return 0;};
    void set_legatura(int x, int y, string str){leg = 1; lit+=str;};
    void make_final(){isf = 1;}


};

class Automata{

    Matricea M[100][100];

    int nr_stari;
    int nr_legaturi;
    Matricea stare_initiala;


public:
    friend class Matricea;
    Automata();
    void creare();
    void mat_begin();
    bool isAutomata(char *cuv);
    Matricea deplasare(string l, int &linie, int &coloana);


};
Automata :: Automata()
{
    creare();
}
void Automata :: mat_begin()
{
    int i, j;
    for(i = 0; i < nr_stari; i++)
        for(j = 0; j < nr_stari; j++)
            {
                M[i][j].isf = 0;
                M[i][j].leg = 0;
                M[i][j].lit = "";
            }
    cout << "\nMatricea de inceput\n";
    for(i = 0; i < nr_stari; i++)
    {
        for(j = 0; j < nr_stari; j++)
        {
            cout <<M[i][j].leg << " ";
        }
        cout<<"\n";
    }
}


void Automata :: creare()
{
    int n, i, j, x, y, m, sf;
    f >> n;

    f >> m;
    cout << "La creare: \n\n";
    nr_legaturi = m;
    nr_stari = n;
    cout<<"nr stari: "<<nr_stari;
    cout<<"\nnr leg: "<<nr_legaturi;
    char s[50];
    mat_begin();
    for(i = 0; i < m; i++)
    {
        f >> x >> y >> s;
        M[x][y].set_legatura(x,y,s);


    }


    f >> sf;
    for(i = 0; i < sf; i++){
        f >> x;
        for(j = 0; j < n; j++)
            M[j][x].make_final();
    }
    cout << "\n\nMatricea cu muchii\n\n";
    for(i = 0; i < nr_stari; i++)
    {
        for(j = 0; j < nr_stari; j++)
        {
            cout <<M[i][j].leg << " ";
        }
        cout<<"\n";
    }
    stare_initiala = M[0][0];

}
Matricea Automata :: deplasare(string l, int &linie, int &coloana)
{
    int i,j,n,m;
    n = nr_stari;
    m = nr_legaturi;
    cout << "\nLa deplasare, n = "<<n;
    for(i = 0; i < n; i++)
        if(M[linie][i].is_leg() == 1){
            cout << "\n legatura intre " << linie <<" si "<<i<<"\n";
            cout << "l = "<<l<<"\n";
            cout << "lit = "<<M[linie][i].lit<<"\n";
            cout <<"\nRezultatul compararii: "<<l.compare(M[linie][i].lit)<<"\n";
            cout <<"\nRezultatul cautarii: "<<M[linie][i].lit.find(l)<<"\n";

            if(M[linie][i].lit.find(l) == 0){

                coloana = i;
                return M[linie][i];


            }
        }
    return M[linie][coloana];
}

bool Automata :: isAutomata(char *cuv)
{
    int i, j, n, m, ii, jj, nr;
    string cuw;
    n = nr_stari;
    m = nr_legaturi;
    nr = strlen(cuv);
    Matricea X;
    X = stare_initiala;
    cout << "\nLa isAutomata\n\n";
    cout << "nr/lung cuv este: "<<nr<<"\n";
    ii = 0;
    jj = 0;//linia si coloana pt starea 0
    for(i = 0; i < nr; i++){

        if(cuv[i] != ' ')
        {
            cuw.push_back(cuv[i]);
            cout << "cuw inainte de erase: "<<cuw<<"\n";
            X = deplasare(cuw, ii, jj);
            cuw.erase();
            cout << "cuw dupa de erase: "<<cuw<<"\n";
            ii = jj;
        }

        if(cuv[i] == ' ')
        {
            j = i;
            while(cuv[j+1] != ' ')
            {
                cuw.push_back(cuv[j+1]);
                j++;
            }
            X = deplasare(cuw, ii, jj);
            cuw.erase();
            ii = jj;
            j++;
        }
    }
    if(X.is_final()==1)
        return 1;
    return 0;




}
int main()
{
    int a,i;
    char cuvant[50];
   // cin >> a;
   // for(i = 0; i < a;i++)
    cin.get(cuvant,50);

    Automata X;

    //if(a == 0)
       // if(X.M[0][0].is_final()==1)
         //   cout << 1;
    cout << X.isAutomata(cuvant);

    return 0;
}
